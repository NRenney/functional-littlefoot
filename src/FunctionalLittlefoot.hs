module FunctionalLittlefoot where

import AST
import Data.Int

elit = ELit . LInt
elitf = ELit . LFloat
elitb = ELit . LByte


mkFnCall :: String -> [Expr] -> Expr
mkFnCall = EFunCall

mkFnCallS :: String -> [Expr] -> Statement
mkFnCallS name = SExpr . mkFnCall name

mkFillRect :: Expr -> Expr -> Expr -> Expr -> Expr -> Statement
mkFillRect e1 e2 e3 e4 e5 = SExpr (mkFnCall "fillRect" [e1, e2, e3, e4, e5])

--A couple of function calls required in the program
clearDisplay = mkFnCallS "clearDisplay" []

scaleXTouch = SInit TInt "xPos" (ECast TInt (EBinOp BOpMult (elitf 7.5)(EVar "x")))
scaleYTouch = SInit TInt "yPos" (ECast TInt (EBinOp BOpMult (elitf 7.5)(EVar "y")))

scaleTouch = [scaleXTouch] ++ [scaleYTouch]

mkInitialise :: [Statement] -> Def
mkInitialise = DFunc TVoid "initialise" []

mkRepaint :: [Statement] -> Def
mkRepaint ss = DFunc TVoid "repaint" [] ([clearDisplay] ++ ss)

mkTouchStartEvent :: [Statement] -> Def
mkTouchStartEvent ss = DFunc TVoid "touchStart" [
                                    (TInt, "index"),
                                    (TFloat, "x"),
                                    (TFloat, "y"),
                                    (TFloat, "z"),
                                    (TFloat, "vz")
                                    ] (scaleTouch ++ ss)

mkTouchMoveEvent :: [Statement] -> Def
mkTouchMoveEvent ss = DFunc TVoid "touchMove" [
                                    (TInt, "index"),
                                    (TFloat, "x"),
                                    (TFloat, "y"),
                                    (TFloat, "z"),
                                    (TFloat, "vz")
                                    ] (scaleTouch ++ ss)
                                    
mkTouchEndEvent :: [Statement] -> Def
mkTouchEndEvent ss = DFunc TVoid "touchEnd" [
                                    (TInt, "index"),
                                    (TFloat, "x"),
                                    (TFloat, "y"),
                                    (TFloat, "z"),
                                    (TFloat, "vz")
                                    ] (scaleTouch ++ ss)

mkHitTest :: [Statement] -> Def
mkHitTest = DFunc TVoid "hitTest" [
                                            (TInt, "x"),
                                            (TInt, "y"),
                                            (TInt, "w"),
                                            (TInt, "h")
                                            ]
gte = EBinOp BOpGTE
lte = EBinOp BOpLTE
binAnd = EBinOp BOpAND

hitTest :: Int32 -> Int32 -> Int32 -> Int32 -> [Statement] -> Statement
hitTest  x y w h ss = SIf (gte (EVar "xPos") (elit x) `binAnd`
            lte (EVar "xPos") (elit (x + w)) `binAnd`
            gte (EVar "yPos") (elit y) `binAnd`
            lte (EVar "yPos") (elit (y + h))
            ) ss

