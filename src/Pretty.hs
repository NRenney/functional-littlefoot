module Pretty where

import qualified Text.PrettyPrint.ANSI.Leijen as PP

import Data.Int
import AST

------------------------------------------------------------------------------
-- pretty printing class
------------------------------------------------------------------------------

class Pretty a where
  pretty :: a -> PP.Doc

  prettyS :: a -> String
  prettyS = show . pretty

------------------------------------------------------------------------------
-- types
------------------------------------------------------------------------------

tipe :: Type -> PP.Doc
tipe TInt   = PP.text "int"
tipe TFloat = PP.text "float"
tipe TBool  = PP.text "bool"
tipe TVoid  = PP.text "void"

instance Pretty Type where
  pretty = tipe

------------------------------------------------------------------------------
-- literal
------------------------------------------------------------------------------

literal :: Literal -> PP.Doc
literal (LInt i)   = PP.integer $ toInteger i
literal (LFloat f) = PP.float f
literal (LBool b)  = if b then PP.text "true" else PP.text "false"
literal (LByte b)  = PP.integer $ toInteger b

instance Pretty Literal where
  pretty = literal

------------------------------------------------------------------------------
-- expressions
------------------------------------------------------------------------------

binop :: BinOp -> PP.Doc
binop BOpPlus   = PP.text "+"
binop BOpMinus  = PP.text "-"
binop BOpMod    = PP.text "%"
binop BOpDiv    = PP.text "/"
binop BOpMult   = PP.text "*"
binop BOpEqual  = PP.text "=="
binop BOpL      = PP.text "<"
binop BOpLTE    = PP.text "<="
binop BOpG      = PP.text ">"
binop BOpGTE    = PP.text ">="
binop BOpNE     = PP.text "!="
binop BOpAND    = PP.text "&&"
binop BOpOR     = PP.text "||"
binop BOpBitAnd = PP.text "&"
binop BOpBitOr  = PP.text "|"

unaryop :: UnaryOp -> PP.Doc
unaryop UOpNot = PP.text "!"
unaryop UOpBitNot = PP.text "~"

expr :: Expr -> PP.Doc
expr (ELit l)           = literal l
expr (EBinOp bop le re) = expr le PP.<+> binop bop PP.<+> expr re
expr (EUnaryOp uop e)   = unaryop uop PP.<> expr e

expr (ECast t e)        =
  tipe t PP.<> PP.char '(' PP.<> expr e PP.<> PP.char ')'

expr (EFunCall name es) = PP.text name PP.<> (PP.tupled $ map expr es)
expr (EVar name) = PP.text name

instance Pretty Expr where
  pretty = expr

------------------------------------------------------------------------------
-- statements
------------------------------------------------------------------------------

statement :: Statement -> PP.Doc
statement (SIf ce ss) =
          PP.nest 2 (PP.text "if" PP.<> (PP.parens (expr ce))
          PP.<> PP.char '{'
          PP.<$> PP.vcat (map (\s -> statement s PP.<> PP.char ';') ss))
          PP.<$> PP.char '}'

statement (SFor ie ce pe ss) = undefined

statement (SWhile ce ss) =
            PP.nest 2 (PP.text "while" PP.<+> PP.char '(' PP.<+> expr ce PP.<+> PP.char ')'
            PP.<$> PP.char '{' PP.<$>
            PP.vcat (map (\s -> statement s PP.<> PP.char ';') ss))
            PP.<$> PP.char '}'

statement (SExpr e)      = expr e
statement (SReturn e)    = PP.text "return" PP.<+> expr e

statement (SAssign name e) = PP.text name PP.<+> PP.char '=' PP.<+> expr e
statement (SDecl t name)   = tipe t PP.<+> PP.text name
statement (SInit t name e) = tipe t PP.<+> PP.text name PP.<+> PP.char '=' PP.<+> expr e

terminator :: Statement -> PP.Doc
terminator (SIf _ _) = PP.empty
terminator (SWhile _ _) = PP.empty
terminator _ = PP.char ';'

instance Pretty Statement where
  pretty = statement

------------------------------------------------------------------------------
-- definitions
------------------------------------------------------------------------------

def :: Def -> PP.Doc
def (DFunc t name args ss) =
    tipe t PP.<+> PP.text name
    PP.<> PP.tupled (map (\(t,n) -> tipe t PP.<+> PP.text n) args)
    PP.<$> PP.char '{' PP.<>
            PP.nest 4 (PP.line PP.<>
            PP.vcat (map (\s -> statement s PP.<> terminator s) ss ) )
    PP.<$> PP.char '}'

def (DGlobal t name) = tipe t PP.<+> PP.text name PP.<> PP.char ';'

instance Pretty Def where
  pretty = def

defs :: Program -> PP.Doc
defs (Program defs) = PP.vcat (map def defs)

instance Pretty Program where
  pretty = defs
