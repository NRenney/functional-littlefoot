module AST where

import Data.Int
import Data.Word

--ToDo Add strings and lits?
data Type =
      TInt
    | TFloat
    | TBool
    | TVoid

-------------------------------------------------------------------------------

data Literal =
      LInt Int32
    | LFloat Float
    | LBool Bool
    | LByte Word8

data BinOp =
      BOpPlus
    | BOpMinus
    | BOpMod
    | BOpDiv
    | BOpMult
    | BOpEqual
    | BOpL
    | BOpLTE
    | BOpG
    | BOpGTE
    | BOpNE
    | BOpAND
    | BOpOR
    | BOpBitAnd
    | BOpBitOr

data UnaryOp =
      UOpNot
    | UOpBitNot

data Expr =
      ELit Literal
    | EBinOp BinOp Expr Expr
    | EUnaryOp UnaryOp Expr
    | ECast Type Expr
    | EFunCall String [Expr]
    | EVar String

------------------------------------------------------------------------------
-- statements
------------------------------------------------------------------------------

data Statement =
      SIf Expr [Statement]
    | SFor Expr Expr Expr [Statement]
    | SWhile Expr [Statement]
    | SExpr Expr
    | SReturn Expr
    | SAssign String Expr
    | SDecl Type String
    | SInit Type String Expr

------------------------------------------------------------------------------
-- definitions
------------------------------------------------------------------------------

data Def =
    DFunc Type String [(Type, String)] [Statement]
  | DGlobal Type String

newtype Program = Program [Def]
