# Functional-Littlefoot

A simple eDSL for Roli's blocks language littlefoot written and embedded in Haskell.

Pretty prints littlefoot.

____
## Useage

To Build:

```stack build```

To run: 

```stack exec Functional-Littlefoot-exe```
