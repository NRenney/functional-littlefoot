module Main where

import AST
import Data.Int
import qualified Pretty as P
import qualified FunctionalLittlefoot as FL

ten = FL.elit 10

block = FL.mkFillRect ten ten ten ten ten

hitter = FL.hitTest 11  11 11 11 [block]

main :: IO ()
main = putStrLn (P.prettyS hitter)
